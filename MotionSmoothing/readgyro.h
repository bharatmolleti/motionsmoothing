#ifndef __READ_GYRO__
#define __READ_GYRO__

typedef struct{
	double* timestamp; 
	double* timegap;
	double** anglev; // angleX, angleY, angleZ
	double firststamp;

	int ntimestamp;
	int ntimegap;
	int nanglev;
} GyroData;

class GyroDataReader{
public:
	GyroData* readGyro(const char* sFileName);
	GyroDataReader();
	~GyroDataReader();

private:
	GyroData* gyro_;

	int allocateGyroDS(int count);
	void clearGyroDS();
};

#endif // __READ_GYRO__