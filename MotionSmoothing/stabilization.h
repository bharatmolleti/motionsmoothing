#ifndef __STABILIZATION_H__
#define __STABILIZATION_H__

typedef struct CameraParamStruct{
		double ts;	// read out time of rolling shutter camera (usually, less than the frameduration)
		double td; // delay between timestamps of gyro readings and frames.
		double wd[3]; //  drift of gyro readings
		double f; // focal length in pixels
		double w;	// frame height
		double h;	// frame width
		double** K; // camera intrinsic matrix
		double** Kinv; // inverse of camera intrinsic matrix
}CameraParam;

#endif // __STABILIZATION_H__