#include "find_dt.h"
#include <stdlib.h>
#include <stdio.h>

FindDTData* FindDT::find_dt(GyroData* gyro, FindGroupData* gdata, double ta, double tb){
	const double* gyrostamp =  gyro->timestamp;
	const double* gyrogap = gyro->timegap;	
	const double* idxa = gdata->idx;

	FindDTData* dtdata = (FindDTData*)malloc(sizeof(FindDTData));

	// % find the delta_t for  every angular velocity
	int w_num = gdata->nidx;
	//printf("\n w_num is %d, ta is %f, tb is %f", w_num, ta, tb);

	// % get position of ta and tb
	int i = (int)idxa[0];
	//printf("\n i was %d", i);
	while( (gyrostamp[i] < ta) && (i <= idxa[w_num-1]) ){
		i++;
	}

	int tempa = i;
	if (i == 1){
		tempa = 1;
	}

	//printf("\n tempa is %d", tempa);

	int tempb = (int)idxa[w_num-1];

	//printf("\n tempb is %d", tempb);
	
	double* temp = (double*) malloc(sizeof(double) * (tempb-tempa));
	int j = 0;
	for(int i = tempa; i <= tempb ; i++, j++){
		temp[j] = gyrogap[i-1];
		//printf("\ntemp[%d] = %0.4f, i = %d", j, temp[j], i);
	}
	//printf("\n gyroscope tempa is %f", gyrostamp[tempa]);
	temp[0] = gyrostamp[tempa] - ta;
	temp[j-1] = tb - gyrostamp[tempb-1];
	//printf("\nlast value is %f , gyrostamp tempb is %f", temp[j-1], gyrostamp[tempb-1]);

	dtdata->dt = temp;
	dtdata->ndt = j;

	return dtdata;
}