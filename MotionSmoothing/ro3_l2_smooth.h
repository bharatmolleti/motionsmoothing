#ifndef __RO3_L2_SMOOTH_H__
#define __RO3_L2_SMOOTH_H__

#include "gyrointeg.h"	// for GyroIntegData
#include "readgyro.h" // for GyroData

typedef struct{
	double*** newrotation;
	int nnewrotation;

	double* costvalue;
	int ncostvalue;
}ro3_l2_smooth_data;

class ro3_l2_smoooth{
public:
	ro3_l2_smooth_data* ro3_l2_smooth(GyroData* gyro, int constant, GryoIntegData* oldrotation);
};

#endif // __RO3_L2_SMOOTH_H__