#include "readgyro.h"

#include <stdio.h>
#include <stdlib.h>

const int maxLineLength = 256;

const int ANGLEVCOUNT = 3;

const int WORD_LENGTH = 15;

const int DELIMITERCOUNT = 4;

const int ERROR = -1;
const int OK = 0;

GyroDataReader::GyroDataReader(){
	gyro_ = NULL;
}

GyroDataReader::~GyroDataReader(){
	clearGyroDS();
}

int GyroDataReader::allocateGyroDS(int count){
	gyro_ = new GyroData();

	gyro_->timestamp = new double[count];
	gyro_->timegap = new double[count];
	gyro_->anglev = new double*[count];

	for(int i = 0; i < count; i++){
		gyro_->anglev[i] = new double[ANGLEVCOUNT];
	}

	gyro_->ntimestamp = count;
	gyro_->ntimegap = (count-1);
	gyro_->nanglev = count;

	gyro_->firststamp = 0;

	return OK;
}

void GyroDataReader::clearGyroDS(){
	if(gyro_){
		if(gyro_->timestamp) delete[] gyro_->timestamp;
		if(gyro_->timegap) delete[] gyro_->timegap;
		if(gyro_->anglev) {
			for(int i = 0; i < gyro_->nanglev; i++){
				delete[] gyro_->anglev[i];
			}
			delete[] gyro_->anglev;
		}
		delete gyro_;
		gyro_ = NULL;
	}	
}

GyroData* GyroDataReader::readGyro(const char* sFileName){
	if(sFileName == NULL){
		printf("\nERROR: Needed File is NULL");
		return NULL;
	}
		
	FILE* fp = fopen(sFileName, "rb");
	if(fp == NULL){
		printf("\nERROR: Needed File cannot be opened");
		return NULL;
	}

	char line[maxLineLength];

	// find the number of readings
	int linecount = 0;	
	while(!feof(fp)){
		fscanf( fp, "%s", line);
		linecount++;
	}

	//printf("\n The File : %s has %d lines", sFileName, linecount);

	// allocate the ds
	if(OK != allocateGyroDS(linecount)){
		printf("\nERROR: Memory allocation failure!");
		return NULL;
	};

	// fill up the ds
	char tempWord[WORD_LENGTH];
	const char DELIMITER = ',';

	int nCounter = 0;
	fseek(fp, 0, SEEK_SET);
	while(!feof(fp)){
		fscanf( fp, "%s", line);
		//printf("\n Read line : %s ", line);
		
		int lineposition = 0;
		int delimiterCount = 0;
		int position = 0;
		while(delimiterCount < DELIMITERCOUNT){			
			tempWord[position] = line[lineposition];
			if(tempWord[position] == DELIMITER){
				tempWord[position] = '\0';
				//printf("\n For Count : %d extracted : %s", delimiterCount, tempWord);
				switch(delimiterCount){
					case 0:
						gyro_->timestamp[nCounter] = atof(tempWord);
						break;
					case 1:
						gyro_->anglev[nCounter][0] = atof(tempWord);
						break;
					case 2:
						gyro_->anglev[nCounter][1] = atof(tempWord);
						break;
					case 3:
						gyro_->anglev[nCounter][2] = atof(tempWord);
						break;
					default:
						break;
				}
				delimiterCount++;
				position = 0;
			}else{
				position++;
			}
			lineposition ++;			
		}
		//printf("\n Read in values : (%d), %f, %f, %f, %f", nCounter,
		//							gyro_->timestamp[nCounter], gyro_->anglev[nCounter][0],
		//							gyro_->anglev[nCounter][1],	gyro_->anglev[nCounter][2]);
		if(nCounter>0){
			gyro_->timegap[nCounter-1] = gyro_->timestamp[nCounter]-gyro_->timestamp[nCounter-1];
			//printf(", %f", gyro_->timegap[nCounter-1]);
		}else{
			gyro_->firststamp = gyro_->timestamp[0];
		}
		//break;
		nCounter++;
	}

	gyro_->ntimestamp = nCounter;
	gyro_->nanglev = nCounter;
	gyro_->ntimegap = gyro_->ntimestamp -1;

	fclose(fp);

	return gyro_;
}	