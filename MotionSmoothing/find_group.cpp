#include "find_group.h"
#include <stdlib.h>

FindGroupData*  FindGroup::find_group(double* gyrostamp, double ta, double tb, int idxstart){

	// % find the gyro reading index group around the given time ta, and before tb

	int i = idxstart; // starts from 1?

	while(gyrostamp[i-1] < ta){
		i++;
	}

	int tempa = 0;
	if(i>1){
		tempa = i-1;
	}else{
		tempa = i;
	}

	while(gyrostamp[i-1]<tb){
		i++;
	}

	int tempb = i-2;

	FindGroupData* data = (FindGroupData*) malloc(sizeof(FindGroupData));
	data->nidx = tempb - tempa + 1;
	data->idx = (double*) malloc(sizeof(double) * (data->nidx + 2));
	for(int i = tempa, j = 0; i <= tempb; i++, j++){
		data->idx[j] = i;
	}
	data->idxstart = tempb +1;

	return data;
}