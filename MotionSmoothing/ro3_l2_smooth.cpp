#include "ro3_l2_smooth.h"

#include "matrixutils.h"
#include <stdio.h>
#include <cmath>

class SquareMatrix{
private:
	double** matrix;
	int size;
public:
	SquareMatrix(int n){
		size = n;
		matrix = (double**) malloc(sizeof(double*) * size);
		for(int i = 0; i < size; i++){
			matrix[i] = (double*) malloc(sizeof(double) * size);
		}
	}

	SquareMatrix(int n, int initValue){
		size = n;
		matrix = (double**) malloc(sizeof(double*) * size);
		for(int i = 0; i < size; i++){
			matrix[i] = (double*) malloc(sizeof(double) * size);
			for(int j = 0; j < size; j++){
				matrix[i][j] = initValue;
			}
		}
	}

	SquareMatrix(const SquareMatrix& m){
		size = m.size;
		matrix = (double**) malloc(sizeof(double*) * size);
		for(int i = 0; i < size; i++){
			matrix[i] = (double*) malloc(sizeof(double) * size);
			for(int j = 0; j <size; j++){
				matrix[i][j] = m.matrix[i][j];
			}
		}
	}

	double** getArray() const{
		return matrix;
	}

	int getSize() const{
		return size;
	}

	void log(){
		// Step 1 : (A- A')/2
		SquareMatrix sm_temp(*this);
		SquareMatrix sm_transpose(*this);
		double** temp = sm_temp.getArray();
		double** transpose = sm_transpose.getArray();
		Transpose(transpose, size);
		for(int i = 0; i < size; i++){
			for(int j = 0; j< size; j++){
				temp[i][j] = (temp[i][j] - transpose[i][j])/2;
			}
		}
		//printf("\n (A-A')/2");
		//sm_temp.print(); // you should get the values of (A-A')/2;

		// Step 2: finding || z ||
		SquareMatrix sm_absolute(sm_temp);
		double** absolute = sm_absolute.getArray();
		for(int i = 0; i < sm_absolute.getSize(); i++){
			for(int j = 0; j < sm_absolute.getSize(); j++){
				absolute[i][j] = abs(absolute[i][j]);
			}
		}
		//printf("\n ||z|| is ");
		//sm_absolute.print();

		// Step 3: finding asin(||z||)
		SquareMatrix sm_sinabs(sm_absolute);
		double** sinabs = sm_sinabs.getArray();
		for(int i = 0; i < sm_sinabs.getSize(); i++){
			for(int j = 0; j < sm_sinabs.getSize(); j++){
				sinabs[i][j] = asin(sinabs[i][j]);
			}
		}
		//printf("\n asin ||z|| is");
		//sm_sinabs.print();

		SquareMatrix sm_absolute_inverse(3);
		double** absolute_inverse = sm_absolute_inverse.getArray();
		Inverse(absolute, 3, absolute_inverse);

		Multiply(sinabs, absolute_inverse, 3, transpose);
		Multiply(transpose, temp, 3, matrix);
	
		//printf("\n Needed log values are : ");
		//print();		
	}// ends log
	
	void print(){
		if(!matrix)  return;

		for(int i = 0; i < size; i++){
			printf("\n");
			for(int j = 0; j < size; j++){
				printf("% 3.4f  ", matrix[i][j]);
			}			
		}
		printf("\n");
	}

	SquareMatrix(int n, double** in){
		size = n;
		matrix = (double**) malloc(sizeof(double*) * size);
		for(int i = 0; i < size; i++){
			matrix[i] = (double*) malloc(sizeof(double) * size);
			for(int j =0; j < size; j++){
				matrix[i][j] = in[i][j];
			}// for j
		}// for i
	}// SquareMatrix(double** in, int n)

	~SquareMatrix(){
		if(matrix){
			for(int i = 0; i < size; i++){
				if(matrix[i]){
					free (matrix[i]);
				}				
			}// for loop
			free(matrix);
		}// matrix if
	}// ~SquareMatrix
};


ro3_l2_smooth_data* ro3_l2_smoooth::ro3_l2_smooth
		(GyroData* gyro, 
		int coeff, 
		GryoIntegData* oldrotation){
		
	// % smoothing function of a sequence of rotation matrices using manifold gradient descent, 
	// % the smooth term is L2 norm manifold difference || logm(A' * b)||
	// % coeff is the weight of the L2 smoothing term.

	int frame_num = oldrotation->noldrotation;
	printf("\n Obtained frame_num to be %d ", frame_num);

	int max_iter_num = 10;

	// % max reimannian distance constraint
	double max_dis = 0.11; 

	GryoIntegData initrotation(*oldrotation);
	GryoIntegData newrotation(initrotation);

	// % Armijo parameters
	double beta = 0.5;
	double sigma = 0.1;

	printf("\n starting smoothing using manifold optimization .. \n");
	for(int i =0; i < max_iter_num; i++){
		GryoIntegData copyrotation(newrotation);

		// %% Compute the gradient
		printf("\n Computing the gradient ");
		GryoIntegData grad_f(GAT_ZERO, frame_num);
		
		// % first matrix
		{
			SquareMatrix sm_x(3, copyrotation.oldrotation[0]);
			double** x = sm_x.getArray();
			printf("\n The matrix x is ");
			sm_x.print();

			SquareMatrix sm_p(3, oldrotation->oldrotation[0]);
			double** p = sm_p.getArray();
			//printf("\n The matrix p is ");
			//sm_p.print();

			SquareMatrix sm_xp1(3, copyrotation.oldrotation[1]);
			double** xp1 = sm_xp1.getArray();
			//printf("\n The matrix xp1 is ");
			//sm_xp1.print();		

			// for p'*x
			Transpose(p, 3);
			printf("\n The matrix p transpose is ");
			sm_p.print();

			SquareMatrix sm_temp(3);
			double** temp = sm_temp.getArray();
			
			Multiply(p, x, 3, temp);
			printf("\n matrix p transpose multiplies x is \n");
			sm_temp.print();

			// for logm (p'*x)
			int isValidIdentity = isIdentity(temp, 3);
			if(isValidIdentity == 1){
				printf("\n Temp is an Identity matrix");
			}else{
				// thank god for the paper On the BCH formula in so(3), kenth engo rules.. :)
				// its in your dropbox in VideoStab/On the BCH Formula in so(3)
				sm_temp.log();
			}

			// for xp1'*x
			Transpose(xp1, 3);
			//printf("\n The matrix xp1 transpose is ");
			//sm_xp1.print();
			
			SquareMatrix sm_temp2(3);
			double** temp2 = sm_temp2.getArray();

			Multiply(xp1, x, 3, temp2);
			
			sm_temp2.log();

			// final result
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					temp2[i][j] =  (isValidIdentity==1?0:temp[i][j])+(coeff * temp2[i][j]);
				}
			}
			
			grad_f.copyValue(0, temp2);
			
			printf("\n The needed result");
			grad_f.printValueAt(0);
		}
		// % last matrix
		{
			SquareMatrix sm_x(3, copyrotation.oldrotation[copyrotation.noldrotation -1]);
			double** x = sm_x.getArray();
			printf("\n The matrix x is ");
			sm_x.print();

			SquareMatrix sm_p(3, oldrotation->oldrotation[copyrotation.noldrotation -1]);
			double** p = sm_p.getArray();
			//printf("\n The matrix p is ");
			//sm_p.print();

			SquareMatrix sm_xm1(3, copyrotation.oldrotation[copyrotation.noldrotation - 2]);
			double** xm1 = sm_xm1.getArray();
			//printf("\n The matrix xp1 is ");
			//sm_xp1.print();		

			// for p'*x
			Transpose(p, 3);
			printf("\n The matrix p transpose is ");
			sm_p.print();

			SquareMatrix sm_temp(3);
			double** temp = sm_temp.getArray();
			
			Multiply(p, x, 3, temp);
			printf("\n matrix p transpose multiplies x is \n");
			sm_temp.print();

			// for logm (p'*x)
			int isValidIdentity = isIdentity(temp, 3);
			if(isValidIdentity == 1){
				printf("\n Temp is an Identity matrix");
			}else{
				// thank god for the paper On the BCH formula in so(3), kenth engo rules.. :)
				// its in your dropbox in VideoStab/On the BCH Formula in so(3)
				sm_temp.log();
			}

			// for xm1'*x
			Transpose(xm1, 3);
			//printf("\n The matrix xp1 transpose is ");
			//sm_xp1.print();
			
			SquareMatrix sm_temp2(3);
			double** temp2 = sm_temp2.getArray();

			Multiply(xm1, x, 3, temp2);
			
			sm_temp2.log();

			// final result
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					temp2[i][j] =  (isValidIdentity==1?0:temp[i][j])+(coeff * temp2[i][j]);
				}
			}
			
			grad_f.copyValue((grad_f.noldrotation -1), temp2);
			
			printf("\n The needed result");
			grad_f.printValueAt((grad_f.noldrotation -1));
		}
		
		// % others
		for( int j = 1; j < (frame_num - 1); j++){
			SquareMatrix sm_x(3, copyrotation.oldrotation[j]);
			double** x = sm_x.getArray();
			//printf("\n The matrix x is ");
			//sm_x.print();

			SquareMatrix sm_xm1(3, copyrotation.oldrotation[j-1]);
			double** xm1 = sm_xm1.getArray();
			//printf("\n The matrix xm1 is ");
			//sm_xm1.print();

			SquareMatrix sm_xp1(3, copyrotation.oldrotation[j+1]);
			double** xp1 = sm_xp1.getArray();
			//printf("\n The matrix xp1 is ");
			//sm_xp1.print();		

			SquareMatrix sm_p(3, oldrotation->oldrotation[j]);
			double** p = sm_p.getArray();
			//printf("\n The matrix p is ");
			//sm_p.print();

			// for p'*x
			Transpose(p, 3);
			//printf("\n The matrix p transpose is ");
			//sm_p.print();

			SquareMatrix sm_temp(3);
			double** temp = sm_temp.getArray();
			
			Multiply(p, x, 3, temp);
			//printf("\n matrix p transpose multiplies x is \n");
			//sm_temp.print();

			// for logm (p'*x)
			int isValidIdentity = isIdentity(temp, 3);
			if(isValidIdentity == 1){
				//printf("\n Temp is an Identity matrix");
			}else{
				// thank god for the paper On the BCH formula in so(3), kenth engo rules.. :)
				// its in your dropbox in VideoStab/On the BCH Formula in so(3)
				sm_temp.log();
				//printf("\n logm of p transpose multiplies x is ");
				//sm_temp.print();
			}	

			// for xm1'*x
			Transpose(xm1, 3);
			//printf("\n The matrix xm1 transpose is ");
			//sm_xp1.print();
			
			SquareMatrix sm_temp3(3);
			double** temp3 = sm_temp3.getArray();

			Multiply(xm1, x, 3, temp3);
			//printf("\n The matrix xm1 transpose multiplies x is ");
			//sm_temp3.print();
			// for logm(xm1'*x)
			sm_temp3.log();
			//printf("\n The log of matrix xm1 transpose multiplies x is ");
			//sm_temp3.print();

			// for xp1'*x
			Transpose(xp1, 3);
			//printf("\n The matrix xp1 transpose is ");
			//sm_xp1.print();
			
			SquareMatrix sm_temp2(3);
			double** temp2 = sm_temp2.getArray();

			Multiply(xp1, x, 3, temp2);
			//printf("\n The matrix xp1 transpose multiplies x is ");
			//sm_temp2.print();

			// for logm(xp1'*x)
			sm_temp2.log();
			//printf("\n The log of matrix xp1 transpose multiplies x is ");
			//sm_temp2.print();

			// final result
			SquareMatrix sm_result(3);
			double** result = sm_result.getArray();
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					result[i][j] =  (isValidIdentity==1?0:temp[i][j])+(coeff * temp2[i][j])+(coeff * temp3[i][j]);
				}
			}
			
			grad_f.copyValue(j, result);
			
			//printf("\n The needed result : %d ", j);
			//grad_f.printValueAt(j);
		}

		printf("\n \n REFINING RESULTS");
		// % refine the accumulated error
		for(int j = 0; j < frame_num; j++){
			double** B = grad_f.oldrotation[j];
			double x = -1 * B[0][1];
			double y = B[0][2];
			double z = -1 * B[1][2];

			B[0][0] = 0;
			B[0][1] = -1 * x;
			B[0][2] = y;

			B[1][0] = x;
			B[1][1] = 0;
			B[1][2] = -1 * z;

			B[2][0] = -1 *y;
			B[2][1] = z;
			B[2][2] = 0;

			//printf("\n The needed result : %d ", j);
			//grad_f.printValueAt(j);
		}

		// Next would be 
		// % Compute the Hessian
		printf("\n Compute Hessian");
		int ngu = (frame_num)*3;
		double* gu = (double*) malloc(sizeof(double)*ngu);
		int guCounter = 0;
		for(int i = 0; i< frame_num; i++){
			double** B = grad_f.oldrotation[i];
			gu[guCounter] = sqrt(2.0)*-1*B[0][1];
			guCounter++;
			gu[guCounter] = sqrt(2.0)*-1*B[2][0];
			guCounter++;
			gu[guCounter] = sqrt(3.0)*-1*B[2][1];	
			guCounter++;			
		}
		//for(int i = 0; i< ngu; i++){
		//	printf("% 04.4f, ", gu[i]);
		//	if(i%6 == 0) printf("\n");
		//}

		SquareMatrix sm_Hess_f(3*frame_num, 0);
		double** Hess_f = sm_Hess_f.getArray();
		for(int j = 0; j < frame_num; j++){
			// calculate hessian distribution.
			SquareMatrix sm_p = SquareMatrix(3, oldrotation->oldrotation[j]);
			double** p = sm_p.getArray();
			printf("\n Obtained the array p to be ");
			sm_p.print();

			SquareMatrix sm_x = SquareMatrix(3, copyrotation.oldrotation[j]);
			double** x = sm_x.getArray();
			printf("\n Obtained the array x to be ");
			sm_x.print();

			Transpose(p, 3);
			printf("\n Transpose of p is ");
			sm_p.print();

			SquareMatrix sm_ptx = SquareMatrix(3);
			double** ptx = sm_ptx.getArray();
			Multiply(p, x, 3, ptx);
			printf("\n p transpose multiplies x is ");
			sm_ptx.print();
			
			int isValidIdentity = isIdentity(ptx, 3);
			if(isValidIdentity == 1){
				printf("\n sm_ptx is an Identity matrix");
			}else{
				// thank god for the paper On the BCH formula in so(3), kenth engo rules.. :)
				// its in your dropbox in VideoStab/On the BCH Formula in so(3)
				sm_ptx.log();
				printf("\n logm of p transpose multiplies x is ");
				sm_ptx.print();
			}	

			sm_ptx.print();
			double r = frobeniusNorm(ptx, 3);
			printf("\n The needed norm is %f", r);

			// % compute the hessian matrix of the function f(x) = 1/2 * ||dist(p, x)||^2
			// % the distance if geodesic distance p and x are on SO(3) manifold.
			// % the H is represented on canonical coordinate on the vector space of xx.
			// % the orthonormal basis is 
			// % E_1 = 1/.sqrt(2) [ 0 1  0; -1 0  0 ; 0 0 0]
			// % E_2 = 1/.sqrt(2) [ 0 0 -1;  0 0  0 ; 1 0 0]
			// % E_3 = 1/.sqrt(2) [ 0 0  0;  0 0 -1 ; 0 1 0]

			// % constant sectional curvature
			double lambda = 1.0/4.0;
			
			
			int a = 3*j;
			Hess_f[a][a] = Hess_f[a][a];
			Hess_f[a][a+1] = Hess_f[a][a+1];
			Hess_f[a][a+2] = Hess_f[a][a+2];

			Hess_f[a+1][a] = Hess_f[a+1][a];
			Hess_f[a+1][a+1] = Hess_f[a+1][a+1];
			Hess_f[a+1][a+2] = Hess_f[a+1][a+2];

			Hess_f[a+2][a] = Hess_f[a+2][a];
			Hess_f[a+2][a+1] = Hess_f[a+2][a+1];
			Hess_f[a+2][a+2] = Hess_f[a+2][a+2];
		}
		printf("\nhi");
		break;
	}

	


	return 0;
}