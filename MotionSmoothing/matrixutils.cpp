#include "matrixutils.h"

#include <stdlib.h>
#include <cmath>
/*
   Recursive definition of determinate using expansion by minors.
*/
double Determinant(double **a,int n)
{
   int i,j,j1,j2;
   double det = 0;
   double **m = NULL;

   if (n < 1) { /* Error */

   } else if (n == 1) { /* Shouldn't get used */
      det = a[0][0];
   } else if (n == 2) {
      det = a[0][0] * a[1][1] - a[1][0] * a[0][1];
   } else {
      det = 0;
      for (j1=0;j1<n;j1++) {
         m = (double**)malloc((n-1)*sizeof(double *));
         for (i=0;i<n-1;i++)
            m[i] = (double*)malloc((n-1)*sizeof(double));
         for (i=1;i<n;i++) {
            j2 = 0;
            for (j=0;j<n;j++) {
               if (j == j1)
                  continue;
               m[i-1][j2] = a[i][j];
               j2++;
            }
         }
         det += pow(-1.0,j1+2.0) * a[0][j1] * Determinant(m,n-1);
         for (i=0;i<n-1;i++)
            free(m[i]);
         free(m);
      }
   }
   return(det);
}

/*
   Find the cofactor matrix of a square matrix
*/
void CoFactor(double **a,int n,double **b)
{
   int i,j,ii,jj,i1,j1;
   double det;
   double **c;

   c = (double**)malloc((n-1)*sizeof(double *));
   for (i=0;i<n-1;i++)
     c[i] = (double*)malloc((n-1)*sizeof(double));

   for (j=0;j<n;j++) {
      for (i=0;i<n;i++) {

         /* Form the adjoint a_ij */
         i1 = 0;
         for (ii=0;ii<n;ii++) {
            if (ii == i)
               continue;
            j1 = 0;
            for (jj=0;jj<n;jj++) {
               if (jj == j)
                  continue;
               c[i1][j1] = a[ii][jj];
               j1++;
            }
            i1++;
         }

         /* Calculate the determinate */
         det = Determinant(c,n-1);

         /* Fill in the elements of the cofactor */
         b[i][j] = pow(-1.0,i+j+2.0) * det;
      }
   }
   for (i=0;i<n-1;i++)
      free(c[i]);
   free(c);
}

/*
   Transpose of a square matrix, do it in place
*/
void Transpose(double **a,int n)
{
   int i,j;
   double tmp;

   for (i=1;i<n;i++) {
      for (j=0;j<i;j++) {
         tmp = a[i][j];
         a[i][j] = a[j][i];
         a[j][i] = tmp;
      }
   }
}

void Copy(double** a, int n, double** b){
	for(int i = 0; i < n ; i++){
		for(int j = 0; j < n; j++){
			b[i][j] = a[i][j];
		}
	}
}

/*
Inverse of a square matrix, do it in place
*/
void Inverse(double** a, int n, double** b){
	double determinant = Determinant(a, n);
	//printf("\n Calculated the determinant to be : %f \n", determinant);
	
	CoFactor(a, n, b);

	Transpose(b, n);

	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			b[i][j] = b[i][j]/determinant;
			//printf("%f ", b[i][j]);
		}
		//printf("\n");
	}
}


void Multiply(double** a, double** b, int n, double** c){		
	for (int row = 0; row < n ; row++){
 		for (int col = 0; col < n ; col++){
			double sum = 0;
			for (int inner = 0; inner < n; inner++)
			{
				sum += a[row][inner] * b[inner][col];
			}
			c[row][col] = sum;
		}
	}
 }

int isIdentity(double** a, int n){	
	for(int i = 0; i < n; i++){		
		for(int  j= 0; j < n; j++){			
			if(i==j){
				if((int)(abs(a[i][j])+0.5) != 1) return 0;
			}else{
				if((int)(abs(a[i][j])+0.5) != 0) return 0;
			}
		}
	}
	return 1;
}

double frobeniusNorm(double** matrix, int size)
{
    double result = 0.0;
    for(int i = 0; i < size; ++i)
    {
        for(int j = 0; j < size; ++j)
        {
            double value = matrix[i][j];
            result += value * value;
        }
    }
    return sqrt(result);
}