#ifndef __READ_TS__
#define __READ_TS__

typedef struct{
	double* timestamp;
	double* timegap;

	int ntimestamp;
	int ntimegap;
}TSData;

class TSDataReader{
public:
	TSData* readTS(const char* sFileName);
	TSDataReader();
	~TSDataReader();

private:
	TSData* ts_;

	int allocateTSDS(int count);
	void clearTSDS();
};

#endif // __READ_TS__