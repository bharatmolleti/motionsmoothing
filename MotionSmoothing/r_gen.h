#ifndef __R_GEN_H__
#define __R_GEN_H__

#include "find_dt.h"

typedef struct{
	double*** r_seq;
	int nr_seq;
}RGenData;

class RGen{
public:
	RGenData* r_gen(GyroData* gyro, FindGroupData* group, FindDTData* dt);
};

#endif // __R_GEN_H__