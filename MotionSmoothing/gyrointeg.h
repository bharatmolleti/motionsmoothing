#ifndef __GYRO_INTEG__
#define __GYRO_INTEG__

#include "stabilization.h" // for CameraParam
#include "readts.h" // for TSData
#include "readgyro.h" // for GyroData

#include <stdio.h>
#include <stdlib.h>

typedef enum{
	GAT_ZERO = 0
}GyroArrayType;

typedef struct GryoIntegDataStruct{
	double*** oldrotation;
	int noldrotation;

	GryoIntegDataStruct(){
		oldrotation = 0;
		noldrotation = 0;
	}

	~GryoIntegDataStruct(){
		cleanup();
	}

	void copyValue(int index, double** value){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				oldrotation[index][i][j] = value[i][j];
			}
		}
	}

	void printValueAt(int index){
		printf("\n");
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				printf("%  9.4f ,", oldrotation[index][i][j]);
			}
			printf("\n");
		}
	}

	void cleanup(){
		if(oldrotation != 0){
			for(int i = 0; i < noldrotation; i++){
				for(int j = 0; j < 3; j++){
					free(oldrotation[i][j]);
				}
				free(oldrotation[i]);
			}
			free(oldrotation);
			oldrotation = 0;
			noldrotation  = 0;
		}
	}

	GryoIntegDataStruct(GyroArrayType type, int n){
		noldrotation = n;
		oldrotation = (double***) malloc(sizeof(double**) * n);
		for(int i = 0; i < n; i++){
			oldrotation[i] = (double**)malloc(sizeof(double*) * 3);
			for(int j = 0; j < 3; j++){
				oldrotation[i][j] = (double*)malloc(sizeof(double) * 3);
				if(type == GAT_ZERO){
					for(int k = 0; k < 3; k++){
						oldrotation[i][j][k] = 0;
					}
				}
			}
		}
	}

	GryoIntegDataStruct(const GryoIntegDataStruct& in){
		noldrotation = in.noldrotation;
		printf("\n Just a reminder, i am being used.. deep copy in progess");		
		oldrotation = (double***) malloc(sizeof(double**) * in.noldrotation);
		for(int i = 0; i < in.noldrotation; i++){
			oldrotation[i] = (double**)malloc(sizeof(double*) * 3);
			for(int j = 0; j < 3; j++){
				oldrotation[i][j] = (double*)malloc(sizeof(double) * 3);
				for(int k = 0; k < 3; k++){
					oldrotation[i][j][k] = in.oldrotation[i][j][k];
				}
			}
		}
	}
}GryoIntegData;

class GyroInteg{
private:
	double*** oldrotation;
	int noldrotation;

public:
	GryoIntegData* gyrointeg(CameraParam* param, TSData* tsdata, GyroData* gyro);
};

#endif // __GYRO_INTEG__