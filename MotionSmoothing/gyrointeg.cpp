#include "gyrointeg.h"

#include <stdio.h>
#include <stdlib.h>

#include "find_group.h" // for find_group
#include "find_dt.h" // for find_dt

#include "r_gen.h"

#include "matrixutils.h"

GryoIntegData* GyroInteg::gyrointeg
		(CameraParam*	para, 
			TSData* tsdata, GyroData*	gyro)
{

	GryoIntegData* data = (GryoIntegData*) malloc(sizeof(GryoIntegData));

	//% integrate gyro readings to get rotation matrices for each frame

	int frame_num = tsdata->ntimestamp;
	
	// allocate the required output matrix
	double*** oldrotation = 0;

	oldrotation = (double***) malloc(sizeof(double**) * frame_num);	

	// fix the framestamps
	double* framestamp = (double*) malloc(sizeof(double) * frame_num);
	for(int i = 0; i < frame_num; i++){
		framestamp[i] = tsdata->timestamp[i] + para->td;
	}

	// %  Get the sequence of old rotation matrices for each frame
	int idxstart = 1;
	// eye(3)
	double** r_pre = (double**) malloc(sizeof(double*) * 3);
	double** r_pre_temp = (double**) malloc(sizeof(double*) * 3);
	for(int i = 0; i < 3; i++){
		r_pre[i] = (double*) malloc(sizeof(double)*3);
		r_pre_temp[i] = (double*) malloc(sizeof(double)*3);
		for(int j = 0; j < 3; j++){
			r_pre[i][j] = (i == j)?1:0;
			r_pre_temp[i][j] = (i == j)?1:0;
		}
	}
	
	double ta = 0;
	double tb = 0;
	FindGroupData* groupdata = 0;
	FindDTData* dtdata = 0;
	
	FindGroup group;
	FindDT dt;

	RGenData* rgendata;
	RGen r_seq;

	for(int i = 0; i < frame_num; i++){
		if(i == 0){
			ta = 0;
		}else{
			ta = framestamp[i-1];
		}
		tb = framestamp[i];
		// findgroup.m
		groupdata = group.find_group(gyro->timestamp, ta, tb, idxstart);

		groupdata->idx[groupdata->nidx] = groupdata->idx[groupdata->nidx-1]+1;
		groupdata->nidx += 1;

		groupdata->idxstart = groupdata->idxstart -1;
		idxstart = groupdata->idxstart;

		// find_dt.m
		dtdata = dt.find_dt( gyro, groupdata, ta, tb);

		// r_gen.m
		rgendata = r_seq.r_gen(gyro, groupdata, dtdata);
		for(int j = 0; j < rgendata->nr_seq; j++){
			Multiply(r_pre, rgendata->r_seq[j], 3, r_pre_temp);
			Copy(r_pre_temp, 3, r_pre);			
		}

		//printf("\n The final needed value for this round : %d \n", i);
		//for(int a = 0; a < 3; a++){
			//printf("\n");
			//for(int b = 0; b < 3; b++){
				//printf(" % 02.4f ", r_pre[a][b]);
			//}
		//}
		oldrotation[i] = (double**) malloc(sizeof(double*) * 3);
		for(int a = 0; a < 3; a++){
			oldrotation[i][a] = (double*) malloc(sizeof(double) * 3);
		}
		Copy(r_pre, 3, oldrotation[i]);
	}

	data->oldrotation = oldrotation;
	data->noldrotation = frame_num;

	// clear all allocated values
	if(framestamp){
		free(framestamp);
		framestamp = 0;
	}

	//if(r_pre){
	//	for(int i = 0; i < 3; i++){
	//		if(r_pre[i] != NULL) free(r_pre[i]);
	//	}
	//	delete r_pre;
	//	r_pre = 0;
	//}
	//
	return data;
}