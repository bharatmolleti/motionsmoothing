#include "r_gen.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "matrixutils.h"

RGenData* RGen::r_gen(GyroData* gyro, FindGroupData* group, FindDTData* dt){
	RGenData* data = (RGenData*) malloc(sizeof(RGenData));

	// invert the sign and multiply with dt.
	int a_num = group->nidx;
	//printf("\n nIdx = %d ", group->nidx);
	double** anglev = (double**) malloc(sizeof(double*) * group->nidx);
	double* theta = (double*) malloc(sizeof(double) * group->nidx);
	int eq_z = 0;
	for(int i = 0; i < group->nidx; i++){
		anglev[i]  = (double*) malloc(sizeof(double) * 3);
		anglev[i][0]  = -1 * gyro->anglev[(int)group->idx[i]-1][0] * dt->dt[i];
		anglev[i][1]  = -1 * gyro->anglev[(int)group->idx[i]-1][1] * dt->dt[i];
		anglev[i][2]  = -1 * gyro->anglev[(int)group->idx[i]-1][2] * dt->dt[i];
		theta[i] = sqrt((anglev[i][0]*anglev[i][0])+(anglev[i][1]*anglev[i][1])+(anglev[i][2]*anglev[i][2]));
		//printf("\n anglev[%2d] = ( %0.4f, %0.4f, %0.4f ) - > %0.4f ", i, anglev[i][0], anglev[i][1], anglev[i][2], theta[i]);
		eq_z = (theta[i] == 0);
		theta[i] += eq_z;
		anglev[i][0] /= theta[i];
		anglev[i][1] /= theta[i];
		anglev[i][2] /= theta[i];
		theta[i] -= eq_z;
		//printf("\n anglev[%2d] = (% 03.4f,% 03.4f,% 03.4f) - > % 02.4f ", i, anglev[i][0], anglev[i][1], anglev[i][2], theta[i]);
	}

	// r_seq
	double** skew_matrix;
	double** skew_matrix_transpose;
	double** skew_matrix_squared_transpose;

	skew_matrix = (double**) malloc(sizeof(double*) * 3);
	skew_matrix_transpose = (double**) malloc(sizeof(double*) * 3);
	skew_matrix_squared_transpose = (double**) malloc(sizeof(double*) * 3);

	for(int i = 0; i < 3; i++){
		skew_matrix[i] = (double*) malloc(sizeof(double) * 3);
		skew_matrix_transpose[i] = (double*) malloc(sizeof(double) * 3);
		skew_matrix_squared_transpose[i] = (double*) malloc(sizeof(double) * 3);
	}

	double* omega = 0;
	double*** r_seq = (double***) malloc(sizeof(double**) * a_num);
	for(int i = 0; i < a_num; i++){
		omega = anglev[i];
		
		skew_matrix[0][0] = 0;
		skew_matrix[0][1] = -1 * omega[2];
		skew_matrix[0][2] = omega[1];

		skew_matrix[1][0] = omega[2];
		skew_matrix[1][1] = 0;
		skew_matrix[1][2] = -1 * omega[0];

		skew_matrix[2][0] = -1 * omega[1];
		skew_matrix[2][1] = omega[0];
		skew_matrix[2][2] = 0;

		Copy(skew_matrix, 3, skew_matrix_transpose);
		Transpose(skew_matrix_transpose, 3);
		Multiply( skew_matrix, skew_matrix, 3, skew_matrix_squared_transpose);
		Transpose(skew_matrix_squared_transpose, 3);

		r_seq[i] = (double** ) malloc(sizeof(double*) * 3);
		for(int j = 0; j < 3; j++){
			r_seq[i][j] = (double*) malloc(sizeof(double) * 3);
			for(int k = 0; k < 3; k++){
				r_seq[i][j][k] = (j == k) +
								 ( sin(theta[i]) * skew_matrix_transpose[j][k] ) +
								 ( ( 1-cos(theta[i]) ) * skew_matrix_squared_transpose[j][k] );
				//printf("% 02.4f ", r_seq[i][j][k]);
			}
			//printf("\n");
		}
		//printf("\n\n");
	}

	data->nr_seq = a_num;
	data->r_seq = r_seq;

	return data;
}