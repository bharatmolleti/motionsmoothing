#include <stdio.h>
#include <stdlib.h>

#include "readgyro.h"
#include "readts.h"

#include "matrixutils.h" // for inverse

#include "stabilization.h" // for the CameraParam array

#include "gyrointeg.h" // for integrating gyroscope data

#include "ro3_l2_smooth.h" // to generate smoothing values from gyro data.

int main(){

	// -----------------
	// assumed constants
	// -----------------
	CameraParam para;
	
	para.ts = 0.0209;
	para.td = 0.1555;

	para.wd[0] = -0.0082;
	para.wd[1] = 0.0052;
	para.wd[2] = 0.0155;

	para.f = 649.2773;
	para.w = 720;
	para.h = 480;

	para.Kinv = (double**)malloc(sizeof(double*)*3);
	for(int i = 0; i < 3; i++){
		para.Kinv[i] = (double*) malloc(sizeof(double) * 3);
	}

	para.K = (double**)malloc(sizeof(double*)*3);
	for(int i = 0; i < 3; i++){
		para.K[i] = (double*) malloc(sizeof(double) * 3);
	}

	para.K[0][0] = para.f;
	para.K[0][1] = 0;
	para.K[0][2] = (0.5 * para.w);

	para.K[1][0] = 0;
	para.K[1][1] = para.f;
	para.K[1][2] = (0.5 * para.h);

	para.K[2][0] = 0;
	para.K[2][1] = 0;
	para.K[2][2] = 1;

	Inverse(para.K, 3, para.Kinv);

	// ------------------------------------
	// read Gyro readings (with timestamps)
	// ------------------------------------
	const char* sGyroFileName = "gyro.txt";

	GyroDataReader gyroreader;

	GyroData* gyro = gyroreader.readGyro(sGyroFileName);

	if(gyro == NULL){
		printf("\nERROR: Unable to parse the gyro values");
		return 0;
	}

	printf("\nObtained %d timestamps, %d timegaps, and %d angles, First TS: %f", 
		gyro->ntimestamp, gyro->ntimegap, gyro->nanglev, gyro->firststamp);

	// ------------------------------------------------------------------
	// % read framestamps (usually its safe to assume a constant framerate)
	// ------------------------------------------------------------------

	const char* sTSFileName = "framestamps.txt";

	TSDataReader tsreader;

	TSData* tsdata = tsreader.readTS(sTSFileName);

	if(tsdata == NULL){
		printf("\nERROR: Unable to parse the time stamps");
		return 0;
	}

	//printf("\nObtained %d timestamps, %d timegaps", tsdata->ntimestamp, tsdata->ntimegap);

	// ---------------------------
	// % remove bias in gyro samples
	// ---------------------------

	for(int i = 0; i < gyro->nanglev; i++){
			gyro->anglev[i][0] += para.wd[0];
			gyro->anglev[i][1] += para.wd[1];
			gyro->anglev[i][2] += para.wd[2];

			//printf("\n (%f, %f, %f)", gyro->anglev[i][0], gyro->anglev[i][1], gyro->anglev[i][2]);
	}

	// ---------------------------
	// % integrate the gyro readings and get the sequence of 3d rotation matrices corressponding to the frames
	// ---------------------------
	GryoIntegData* oldrotation;
	GyroInteg gyrointeg;

	oldrotation = gyrointeg.gyrointeg(&para, tsdata, gyro);

	//printf("\n Gyro Integ Values\n\n");
	//for(int i = 0; i < oldrotation->noldrotation; i++){
	//	printf("\n Rotation Matrix : %d\n", i);
	//	for(int j =0; j < 3; j++){
	//		for(int k = 0; k < 3; k++){
	//			printf("% 2.4f ", oldrotation->oldrotation[i][j][k]);
	//		}
	//		printf("\n");
	//	}
	//}

	// ---------------------------------------------------------------------
	// % stabilize the current path using constrained manifold optimization
	// % currently we constrain the stabilized size
	// ---------------------------------------------------------------------
	ro3_l2_smooth_data* smoothdata;
	ro3_l2_smoooth smooth;

	smoothdata = smooth.ro3_l2_smooth(gyro, 1000, oldrotation);

	return 0;
}