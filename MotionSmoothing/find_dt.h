#ifndef __FIND_DT__
#define __FIND_DT__

#include "readgyro.h"
#include "find_group.h"

typedef struct{
	double* dt;
	int ndt;
}FindDTData;

class FindDT{
public:
	FindDTData* find_dt(GyroData* gyro, FindGroupData* gdata, double ta, double tb);
};

#endif // __FIND_DT__