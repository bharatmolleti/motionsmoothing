#ifndef __MATRIX_UTILS__
#define __MATRIX_UTILS__

/*
   Recursive definition of determinate using expansion by minors.
*/
double Determinant(double **a,int n);

/*
   Find the cofactor matrix of a square matrix
*/
void CoFactor(double **a,int n,double **b);

/*
   Transpose of a square matrix, do it in place
*/
void Transpose(double **a,int n);

/*
   Copy the square matrix A to a square matrix B
*/

void Copy(double** a, int n, double** b);

/*
Inverse of a square matrix, do it in place
*/
void Inverse(double** a, int n, double** b);

void Multiply(double** a, double** b, int n, double** c);

int isIdentity(double** a, int n);

double frobeniusNorm(double** matrix, int size);

#endif // __MATRIX_UTILS__