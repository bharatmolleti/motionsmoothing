#ifndef __FIND_GROUP__
#define __FIND_GROUP__

typedef struct{
	double* idx;
	int nidx;

	int idxstart;
}FindGroupData;

class FindGroup{
public:
	FindGroupData* find_group(double* gyrostamp, double ta, double tb, int idxstart);
};

#endif // __FIND_GROUP__