// MotionSmoothing.cpp : Defines the entry point for the console application.
#include <stdio.h>

#include "readgyro.h"

void test_readGyro(){
	const char* sFileName = "gyro.txt";

	GyroDataReader gyroreader;

	GyroData* gyro = gyroreader.readGyro(sFileName);

	if(gyro == NULL){
		printf("\nERROR: Unable to parse the gyro values");
		return;
	}

	// print all the gyro values.	
	for(int i = 0; i < gyro->ntimestamp; i++){
		printf("\nTS: %f, (%f, %f, %f)", gyro->timestamp[i]
				, gyro->anglev[i][0], gyro->anglev[i][1]
				, gyro->anglev[i][2]);
		if(i>0)
			printf(", %f", gyro->timegap[i-1]);
	}

	printf("\nObtained %d timestamps, %d timegaps, and %d angles, First TS: %f", 
		gyro->ntimestamp, gyro->ntimegap, gyro->nanglev, gyro->firststamp);
}

#include "readts.h"

void test_readTS(){
	const char* sFileName = "framestamps.txt";

	TSDataReader tsreader;

	TSData* tsdata = tsreader.readTS(sFileName);

	if(tsdata == NULL){
		printf("\nERROR: Unable to parse the time stamps");
		return;
	}

	for(int i = 0; i < tsdata->ntimestamp; i++){
		printf("\nTS: %f", tsdata->timestamp[i]);
		if(i>0)
			printf(", %f", tsdata->timegap[i-1]);
	}

	printf("\nObtained %d timestamps, %d timegaps", tsdata->ntimestamp, tsdata->ntimegap);
}

int main(){
	printf("Hello world");

	//test_readGyro();

	test_readTS();

	//origvalues: -0.1833, 0.5767, 0.1222
	//drift: -0.0082, 0.0052, 0.0155
	// corrected: -1.1915, 0.5819, 0.1377
	

	return 0;
}

