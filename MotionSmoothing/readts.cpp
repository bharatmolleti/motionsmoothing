#include "readts.h"

#include <stdio.h>
#include <stdlib.h>

const int maxLineLength = 256;

const int WORD_LENGTH = 15;

const int ERROR = -1;
const int OK = 0;

TSDataReader::TSDataReader(){
	ts_ = NULL;
}

TSDataReader::~TSDataReader(){
	clearTSDS();
}

int TSDataReader::allocateTSDS(int count){
	ts_ = new TSData();

	ts_->timestamp = new double[count];
	ts_->timegap = new double[count];
	
	ts_->ntimestamp = count;
	ts_->ntimegap = (count-1);
	return OK;
}

void TSDataReader::clearTSDS(){
	if(ts_){
		if(ts_->timestamp) delete[] ts_->timestamp;
		if(ts_->timegap) delete[] ts_->timegap;

		delete ts_;
		ts_ = NULL;
	}	
}

TSData* TSDataReader::readTS(const char* sFileName){
	if(sFileName == NULL){
		printf("\nERROR: Needed File is NULL");
		return NULL;
	}
		
	FILE* fp = fopen(sFileName, "rb");
	if(fp == NULL){
		printf("\nERROR: Needed File cannot be opened");
		return NULL;
	}

	char line[maxLineLength];

	// find the number of readings
	int linecount = 0;	
	while(!feof(fp)){
		fscanf( fp, "%s", line);
		linecount++;
	}

	//printf("\n The File : %s has %d lines", sFileName, linecount);

	// allocate the ds
	if(OK != allocateTSDS(linecount)){
		printf("\nERROR: Memory allocation failure!");
		return NULL;
	};

	// fill up the ds
	int nCounter = 0;
	fseek(fp, 0, SEEK_SET);
	while(!feof(fp)){
		fscanf( fp, "%s", line);
		//printf("\n Read line : %s ", line);		
		ts_->timestamp[nCounter] = atof(line);
		//printf("\n Read TS : %f ", ts_->timestamp[nCounter]);
		if(nCounter>0){
			ts_->timegap[nCounter-1] = ts_->timestamp[nCounter]-ts_->timestamp[nCounter-1];
			//printf(", %f", ts_->timegap[nCounter-1]);
		}
		//break;
		nCounter++;
	}

	ts_->ntimestamp = nCounter;
	ts_->ntimegap = ts_->ntimestamp -1;

	fclose(fp);

	return ts_;
}